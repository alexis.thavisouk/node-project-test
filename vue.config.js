module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'locales',
      fallbackLocale: 'locales',
      localeDir: 'locales',
      enableInSFC: 'locales'
    }
  },
  devServer: {
    disableHostCheck: true
  }
}

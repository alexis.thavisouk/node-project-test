import store from '../store'

// merci stak over flow : https://stackoverflow.com/questions/12043187/how-to-check-if-hex-color-is-too-black
function isDarkColor (color) {
  // strip #
  var c = color.substring(1)
  // convert rrggbb to decimal
  var rgb = parseInt(c, 16)
  // extract red
  var r = (rgb >> 16) & 0xff
  // extract green
  var g = (rgb >> 8) & 0xff
  // extract blue
  var b = (rgb >> 0) & 0xff
  // per ITU-R BT.709
  var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b
  return luma < 40
}

function customColorStyle () {
  return {
    'box-shadow': `10px 10px 15px ${store.state.custom.color}`,
    border: '1px solid',
    'border-color': `#dee2e6 ${store.state.custom.color} ${store.state.custom.color} #dee2e6`
  }
}

function getGoodItemAndColorIt (url) {
  document.querySelectorAll('.vsm--link').forEach(queryItem => {
    const hrefQueryItem = queryItem.href.split('#')[1]
    if (url === hrefQueryItem) {
      queryItem.setAttribute('style', `box-shadow: 0px 0px 10px 5px ${store.state.custom.color} inset`)
    } else {
      queryItem.setAttribute('style', 'box-shadow: 0px 0px')
    }
  })
}

function customVsaHiglightColor () {
  return {
    '--vsa-highlight-color': store.state.custom.color
  }
}

function customBgColor () {
  const color = store.state.custom.color
  let textColor = 'black'
  if (isDarkColor(color)) {
    textColor = 'white'
  }
  return {
    'background-color': store.state.custom.color,
    color: textColor
  }
}

function customFontStyle () {
  return {
    'font-family': store.state.custom.font
  }
}

export default {
  customColorStyle,
  customBgColor,
  customFontStyle,
  customVsaHiglightColor,
  getGoodItemAndColorIt
}

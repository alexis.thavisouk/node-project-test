import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import loader from 'vue-ui-preloader'
import VueSimpleAlert from 'vue-simple-alert'
import VueSimpleAccordion from 'vue-simple-accordion'
import 'vue-simple-accordion/dist/vue-simple-accordion.css'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faSlidersH, faSearch, faFileDownload, faFileUpload, faFilter, faUser, faEye, faSignInAlt,
  faChevronLeft, faChevronRight, faEdit, faTrashAlt, faQuestionCircle, faPaintBrush
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faSlidersH, faSearch, faFileDownload, faFileUpload, faFilter,
  faUser, faEye, faSignInAlt, faChevronLeft, faChevronRight,
  faEdit, faTrashAlt, faQuestionCircle, faPaintBrush
)

const plugins = [VueSidebarMenu, VueSimpleAlert, VueSimpleAccordion, loader]
plugins.forEach(item => {
  Vue.use(item)
})
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')

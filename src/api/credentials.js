import APIConfig from './config'
import axios from 'axios'
import APILogin from './login'

function getCred () {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.get(APIConfig.url + '/api/credentials/', { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function getEmailServiceAccount () {
  return new Promise((resolve, reject) => {
    axios.get(APIConfig.url + '/api/email')
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

function changeCred (type, file) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios({
          url: APIConfig.url + '/api/credentials/' + type,
          data: file,
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'content-type': 'multipart/form-data',
            Authorization: headers.Authorization
          }
        })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

export default {
  getCred,
  changeCred,
  getEmailServiceAccount
}

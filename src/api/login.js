import APIConfig from './config'
import axios from 'axios'
import store from '../store'
import router from '../router'

function login (user, pw) {
  const params = new URLSearchParams()
  params.append('mail', user)
  params.append('pw', pw)
  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }
  return new Promise((resolve, reject) => {
    axios.post(APIConfig.url + '/login', params, config)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

function getHeader () {
  return new Promise((resolve, reject) => {
    getAccessToken().then(token => {
      if (token) {
        resolve({
          Authorization: 'Bearer ' + token
        })
      } else { // need to reconect to get new tokens
        router.push('/login')
      }
    })
  })
}

function parseJwt (token) { // merci stak overflow
  var base64Url = token.split('.')[1]
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
  var jsonPayload = decodeURIComponent(Buffer.from(base64, 'base64').toString('ascii').split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
  }).join(''))
  return JSON.parse(jsonPayload)
}

function isTokenExpire (token) {
  if (token) {
    const jwt = parseJwt(token)
    const currentTime = new Date().getTime() / 1000
    return !(jwt.exp && jwt.exp > currentTime)
  }
  return true
}

function getRoleFromToken (token) {
  if (token) {
    return parseJwt(token).roles
  }
  return []
}

function getAccessToken () {
  return new Promise((resolve, reject) => {
    if (!isTokenExpire(store.state.tokens.access_token)) { // access token is not expired
      resolve(store.state.tokens.access_token)
    } else if (!isTokenExpire(store.state.tokens.refresh_token)) { // access token is expired, but not the refresh token
      getNewAccessToken()
        .then(function (response) {
          resolve(store.state.tokens.access_token)
        })
        .catch(function (error) {
          reject(error)
        })
    } else { // access and refresh token are axpired
      resolve(null)
    }
  })
}

function getNewAccessToken () {
  return new Promise((resolve, reject) => {
    getHeaderRefreshToken()
      .then(headers => {
        axios.get(APIConfig.url + '/api/user/token/refresh', { headers })
          .then(function (response) {
            store.commit('saveTokens', response.data)
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function getHeaderRefreshToken () {
  return new Promise((resolve, reject) => {
    resolve({
      Authorization: 'Bearer ' + store.state.tokens.refresh_token
    })
  })
}

export default {
  login,
  getHeader,
  isTokenExpire,
  getRoleFromToken
}

import APIConfig from './config'
import axios from 'axios'

function getSheetsName (payload) {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/sheet/${payload.sheetId}`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

function getAllSheet () {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/sheet/all`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

export default {
  getSheetsName,
  getAllSheet
}

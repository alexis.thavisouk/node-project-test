import APIConfig from './config'
import axios from 'axios'
import APILogin from './login'

function writeAllTagsOnSheet (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios({
          method: 'post',
          url: `${APIConfig.url}/api/write/all/data`,
          params: {
            idTagTemplate: payload.tagTemplateId,
            spreadSheetId: payload.sheetId,
            sheetName: payload.sheetName
          },
          headers
        })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function writeTagTemplateOnSheet (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios({
          method: 'post',
          url: `${APIConfig.url}/api/write/template`,
          params: {
            idTagTemplate: payload.tagTemplateId,
            spreadSheetId: payload.sheetId,
            sheetName: payload.sheetName
          },
          headers
        })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

export default {
  writeAllTagsOnSheet,
  writeTagTemplateOnSheet
}

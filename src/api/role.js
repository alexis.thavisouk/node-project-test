import APIConfig from './config'
import axios from 'axios'
import APILogin from './login'

function getAllRole () {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.get(APIConfig.url + '/api/role/', { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

export default {
  getAllRole
}

export default {
  url: process.env.VUE_APP_DIC_URL_API,
  corsAllowedOrigin: process.env.VUE_APP_DIC_URL_API,
  urlCodeLabs: process.env.VUE_APP_URL_CODELABS
}

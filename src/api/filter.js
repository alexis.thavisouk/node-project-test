import APIConfig from './config'
import APILogin from './login'
import axios from 'axios'

function saveDataTypes (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.post(`${APIConfig.url}/api/type/donnee/save/all`, payload, { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function saveSystemTypes (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.post(`${APIConfig.url}/api/type/systeme/save/all`, payload, { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function saveTagNames (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.post(`${APIConfig.url}/api/nom/tag/save/all`, payload, { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function saveDatasets (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.post(`${APIConfig.url}/api/ensemble/donnee/save/all`, payload, { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

export default {
  saveDataTypes,
  saveDatasets,
  saveTagNames,
  saveSystemTypes
}

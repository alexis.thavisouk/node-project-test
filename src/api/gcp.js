import APIConfig from './config'
import axios from 'axios'

function getTagModelsWithoutProjectName () {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/gcp/tag/templates`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

function getGcpProject () {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/gcp/project`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

// function getTagModels () {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/tags/')
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

function getTDatasets () {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/gcp/datasets`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

function getDataTypes () {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/type/donnee/all`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

function getTypeSystems () {
  return new Promise((resolve, reject) => {
    axios.get(`${APIConfig.url}/api/type/systeme/all`)
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

export default {
  getTagModelsWithoutProjectName,
  getGcpProject,
  // getTagModels,
  getTDatasets,
  getDataTypes,
  getTypeSystems
}

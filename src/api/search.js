import APIConfig from './config'
import axios from 'axios'

function searchDatacatalog (payload) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${APIConfig.url}/api/search/`,
      params: {
        input: payload.search,
        name: payload.name,
        column: payload.col,
        description: payload.description,
        labels: payload.labels,
        policyTag: payload.tagStrategy,
        systemId: payload.system,
        typeId: payload.dataType,
        tagTemplateId: payload.tagModel,
        datasetId: payload.dataset
      }
    })
      .then(function (response) {
        resolve(response.data)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

// function getAllProject () {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/projects')
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

// function getTagModels () {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/tags/')
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

// function getTDatasets () {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/datasets/')
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

export default {
  searchDatacatalog
  // getAllProject,
  // getTagModels,
  // getTDatasets
}

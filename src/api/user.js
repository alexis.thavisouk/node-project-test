import APIConfig from './config'
import axios from 'axios'
import APILogin from './login'

function getAllUser () {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.get(APIConfig.url + '/api/user/', { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function createUser (user) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.put(APIConfig.url + '/api/user/', user, { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function updateUser (user) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.post(APIConfig.url + '/api/user/', user, { headers })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

function deleteUser (user) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios.delete(APIConfig.url + '/api/user/', { headers, data: user })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

export default {
  getAllUser,
  createUser,
  deleteUser,
  updateUser
}

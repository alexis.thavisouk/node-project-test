import APIConfig from './config'
import axios from 'axios'
import APILogin from './login'

function insertTagsFromSheet (payload) {
  return new Promise((resolve, reject) => {
    APILogin.getHeader()
      .then(headers => {
        axios({
          method: 'post',
          url: `${APIConfig.url}/api/insert/`,
          params: {
            idTagTemplate: payload.tagTemplateId,
            spreadSheetId: payload.sheetId,
            sheetName: payload.sheetName
          },
          headers
        })
          .then(function (response) {
            resolve(response.data)
          })
          .catch(function (error) {
            reject(error)
          })
      })
  })
}

// function writeAllTagsOnSheet (payload) {
//   return new Promise((resolve, reject) => {
//     APILogin.getHeader()
//       .then(headers => {
//         axios.get(APIConfig.url + '/api/write/project/' + payload.projectId + '/tag/' + payload.tagTemplateId + '/sheet/id/' + payload.sheetId + '/name/' + payload.sheetName, { headers })
//           .then(function (response) {
//             resolve(response.data)
//           })
//           .catch(function (error) {
//             reject(error)
//           })
//       })
//   })
// }

// function writeTagTemplateOnSheet (payload) {
//   return new Promise((resolve, reject) => {
//     APILogin.getHeader()
//       .then(headers => {
//         axios.get(APIConfig.url + '/api/write-template/project/' + payload.projectId + '/tag/' + payload.tagTemplateId + '/sheet/id/' + payload.sheetId + '/name/' + payload.sheetName, { headers })
//           .then(function (response) {
//             resolve(response.data)
//           })
//           .catch(function (error) {
//             reject(error)
//           })
//       })
//   })
// }

// function getSheetsName (payload) {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/get-sheetsname/project/' + payload.projectId + '/sheet/id/' + payload.sheetId)
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

// function getAllSheet () {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/all-sheet')
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

// function getTagModelsWithoutProjectName () {
//   return new Promise((resolve, reject) => {
//     axios.get(APIConfig.url + '/api/get-tags/')
//       .then(function (response) {
//         resolve(response.data)
//       })
//       .catch(function (error) {
//         reject(error)
//       })
//   })
// }

export default {
  insertTagsFromSheet
  // writeAllTagsOnSheet,
  // writeTagTemplateOnSheet,
  // getSheetsName,
  // getTagModelsWithoutProjectName
  // getAllSheet
}

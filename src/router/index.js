import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Search',
    component: () => import('../views/search/ResultSearch')
  },
  {
    path: '/results',
    name: 'Results',
    component: () => import('../views/insert/Results')
  },
  {
    path: '/details',
    name: 'Details',
    component: () => import('../views/search/Details')
  },
  {
    path: '/credentials',
    name: 'Credentials',
    component: () => import('../views/credentials/Credentials')
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: () => import('../views/login/Login')
  },
  {
    path: '/export',
    name: 'Export',
    component: () => import('../views/export/Export')
  },
  {
    path: '/user',
    name: 'ManageUser',
    component: () => import('../views/user/UserMain')
  },
  {
    path: '/insert',
    name: 'Insert',
    component: () => import('../views/insert/Insert')
  },
  {
    path: '/filter',
    name: 'Filter',
    component: () => import('../views/filter/Filter')
  },
  {
    path: '/custom',
    name: 'Custom',
    component: () => import('../views/custom/CustomAppMain')
  }
]

const router = new VueRouter({
  routes
})

export default router

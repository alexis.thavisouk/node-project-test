import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState({
    storage: window.sessionStorage
  })],
  state: {
    tokens: null,
    roles: [],
    email: null,
    loading: null,
    custom: {
      color: '#000000',
      logo: null,
      font: 'sans-serif'
    }
  },
  mutations: {
    saveTokens: (state, otherTokens) => {
      state.tokens = otherTokens
      if (!otherTokens) { // if otherTokens is null, reset the roles
        state.roles = []
        state.email = null
      }
    },
    saveRoles: (state, otherRoles) => {
      state.roles = otherRoles
    },
    saveLoading: (state, loading) => {
      state.loading = loading
    },
    saveEmail: (state, email) => {
      state.email = email
    },
    saveCustom: (state, custom) => {
      state.custom = custom
    }
  },
  actions: {
  },
  modules: {
  }
})
